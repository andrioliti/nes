module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('roles', [{
      id: 1,
      name: 'admin',
      desc: 'root',
      value: 0,
      'created_at': new Date(),
      'updated_at': new Date(),
      'deleted_at': null,
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('roles', null, {});
  }
};
