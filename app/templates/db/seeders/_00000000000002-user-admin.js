const { utils: securityUtils } = require('../../src/security');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('users', [{
      email: 'admin@site.com',
      login: 'admin',
      password: securityUtils.sha('qwerty'),
      "full_name": 'admin',
      "role_id": 1,
      "created_at": new Date(),
      "updated_at": new Date(),
      "deleted_at": null,
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('users', null, {});
  }
};
