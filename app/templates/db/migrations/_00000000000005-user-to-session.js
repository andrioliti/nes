module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.addColumn('sessions', 'user_id', {
    type: Sequelize.INTEGER,
    allowNull: false,
    validate: {
      notNull: true,
    },
    references: {
      model: 'users',
      key: 'id',
    }
  }),

  down: (queryInterface, Sequelize) => queryInterface.removeColumns('sessions', 'user_id'),
};
