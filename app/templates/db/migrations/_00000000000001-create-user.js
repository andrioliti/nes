'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('users',  {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      fullName: {
        type: Sequelize.STRING,
        field: 'full_name',
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notNull: true,
          isEmail: true,
        },
      },
      login: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notNull: true,
        },
        unique: true,
      },
      password: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notNull: true,
        },
      },
      // Timestamps
      createdAt: { 
        type: Sequelize.DATE,
        field: 'created_at', 
      }, 
      updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at', 
      },
      deletedAt: { 
        type: Sequelize.DATE,
        field: 'deleted_at', 
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('users');
  }
};
