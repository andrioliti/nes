module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('roles', {
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notNull: true,
        },
      }, 
      desc: Sequelize.STRING,
      // id
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      value: {
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
          notNull: true,
        },
        unique: true,
      },
      // Timestamps
      createdAt: { 
        type: Sequelize.DATE,
        field: 'created_at', 
      }, 
      updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at', 
      },
      deletedAt: { 
        type: Sequelize.DATE,
        field: 'deleted_at', 
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('roles');
  }
};
