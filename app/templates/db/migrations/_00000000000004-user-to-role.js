module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.addColumn('users', 'role_id', {
    type: Sequelize.INTEGER,
      references: {
        model: 'roles',
        key: 'id',
      }
    },
  ),

  down: (queryInterface, Sequelize) => queryInterface.removeColumn('users', 'role_id'),
};
