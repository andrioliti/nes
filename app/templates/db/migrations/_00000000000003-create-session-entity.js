module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('sessions', { 
      token: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notNull: true,
        },
      },
      expiredAt: {
        type: Sequelize.DATE,
        field: 'expired_at',
        allowNull: false,
        validate: {
          notNull: true,
        },
      },

      // id
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      // Timestamps
      createdAt: { 
        type: Sequelize.DATE,
        field: 'created_at', 
      }, 
      updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at', 
      },
      deletedAt: { 
        type: Sequelize.DATE,
        field: 'deleted_at', 
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('sessions');
  }
};
