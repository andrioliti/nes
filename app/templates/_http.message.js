module.exports = class HttpMessage {
  constructor(status, msg) {
    this.status = status;
    this.msg = msg;
  }

  getMsg() {
    return this.msg;
  }

  getStatus() {
    return this.status;
  }
};