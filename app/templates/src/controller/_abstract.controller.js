const controllUtils = require('./controller.utils');

module.exports = dao => {
  return {
    update: (req, res, next) => dao.update(req.body)
      .then(entity => controllUtils.handleEntity(entity))
      .then(entity => controllUtils.sendSuccess(res, entity))
      .catch(error => controllUtils.sendError(res, error)),

    findAll: (req, res, next) => dao.findAll(req.query)
      .then(entity => controllUtils.handleEntity(entity))
      .then(entity => controllUtils.sendSuccess(res, entity))
      .catch(error => controllUtils.sendError(res, error)),

    create: (req, res, next) => dao.create(req.body)
      .then(entity => controllUtils.handleEntity(entity))
      .then(entity => controllUtils.sendSuccess(res, entity))
      .catch(error => controllUtils.sendError(res, error)),

    delete: (req, res, next) => dao.delete(req.params.id)
      .then(() => dao.findAll())
      .then(entity => controllUtils.handleEntity(entity))
      .then(entity => controllUtils.sendSuccess(res, entity))
      .catch(error => controllUtils.sendError(res, error)),
  }
};