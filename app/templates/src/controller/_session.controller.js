const controllUtils = require('./controller.utils');
const sessionDao = require('../dao/session.dao');

const abstractDao = require('./abstract.controller');

module.exports = {
	...abstractDao(sessionDao),
}
