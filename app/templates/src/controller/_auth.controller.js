const controllUtils     = require('./controller.utils');
const userDao           = require('../dao/user.dao');
const sessionDao        = require('../dao/session.dao');
const { jwt: jwtUtils } = require('../security');
const HttpMessage       = require('../utils/http.message');
const config            = require('../utils/config.utils.js');

let userData;

const login = (req, res, next) => userDao.findByLogin(req.body.login)
	.then(userDb => {
		if (!userDb)
			throw new HttpMessage(404, 'User not found');
		
		userData = userDb.dataValues;
		if (!userDb.validatePassword(req.body.password, userData.password))
			throw new HttpMessage(401, 'Password not valid');

		return sessionDao.deleteByUserId(userDb.id);	
	})
	.then(ignore => 
		sessionDao.create({
			userId: userData.id,
			token: jwtUtils.generate({
				login: userData.login,
				role: userData.role.name,
				iat: config.expiredTime,
				created: new Date(),
			}),
		expiredAt: new Date(Date.now() + config.expiredTime),
	}))
	.then(entity => ({
		token: entity.token,
		iat: config.expiredTime,
		created: entity.createdAt,
		roleName: userData.role.name,
	}))
	.then(entity => controllUtils.handleEntity(entity))
	.then(entity => controllUtils.sendSuccess(res, entity))
	.catch(error =>  controllUtils.sendError(res, error));

const logout = (req, res, next) => sessionDao.findSessionWithToken(req.body.token)
	.then(session => {
		if (!session)
			throw new HttpMessage(404, 'Session not found.');
			
		if (session.expiredAt <= new Date())
			throw new HttpMessage(401, 'Session expired.');
		
		return sessionDao.deleteByUserId(session.userId);
	}).then(ignore => controllUtils.handleEntity(new HttpMessage(200, 'Logout.')))
	.then(entity => controllUtils.sendSuccess(res, entity))
	.catch(error =>  controllUtils.sendError(res, error));

const validate = (req, res, next) => sessionDao.findSessionWithToken(req.body.token)
	.then(session => {
		if (!session)
			throw  new HttpMessage(404, 'Session not found.');
			
		if (session.expiredAt <= new Date()) 
			throw new HttpMessage(401, 'Session expired.');

		return new HttpMessage(200, 'Token valid.');
	}).then(entity => controllUtils.sendSuccess(res, entity))
	.catch(error =>  controllUtils.sendError(res, error));;

const refresh = (req, res, next) => sessionDao.findSessionWithToken(req.body.token)
	.then(session => {
		if (!session)
			throw  new HttpMessage(404, 'Session not found.');
			
		if (session.expiredAt <= new Date()) 
			throw new HttpMessage(401, 'Session expired.');

		return userDao.findById(session.userId, ['role']);
	})
	.then(user => {
		userData = user.dataValues;
		return sessionDao.deleteByUserId(userData.id);
	}).then(ignore => sessionDao.create({
		userId: userData.id,
		token: jwtUtils.generate({
			login: userData.login,
			role: userData.role.name,
			iat: config.expiredTime,
			created: new Date(),
		}),
		expiredAt: new Date(Date.now() + config.expiredTime),}))
	.then(entity => ({
		token: entity.token,
		iat: config.expiredTime,
		created: entity.createdAt,
		roleName: userData.role.name,
	}))
	.then(entity => controllUtils.handleEntity(entity))
	.then(entity => controllUtils.sendSuccess(res, entity))
	.catch(error =>  controllUtils.sendError(res, error));

module.exports = {
	login,
	logout,
	validate,
	refresh
};