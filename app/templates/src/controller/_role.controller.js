const roleDao = require('../dao/role.dao');

const abstractDao = require('./abstract.controller');

module.exports = {
	...abstractDao(roleDao),
}
