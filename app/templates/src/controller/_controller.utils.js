module.exports = {
  handleEntity: entity => entity,

  sendSuccess: (res, entity) => res.status(200)
    .send(entity),

  sendError: (res, httpMsg) => res.status(httpMsg.status || 500)
    .send(httpMsg || 'deu ruim'),
}