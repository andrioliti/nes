const controllUtils = require('./controller.utils');
const userDao = require('../dao/user.dao');

const abstractDao = require('./abstract.controller');

module.exports = {
	...abstractDao(userDao),

	findAll: (req, res, next) => userDao.findAll(req.query)
		.then(entity =>  controllUtils.handleEntity(entity))
		.then(entity => entity.map((it) => {
			delete(it.dataValues.password);
			return it;
		}))
		.then(entity =>  controllUtils.sendSuccess(res, entity))
		.catch(error =>  controllUtils.sendError(res, error)),
			
	getAllSession: (req, res, next) => userDao.getAllSession(req.params.id)
			.then(entity =>  controllUtils.handleEntity(entity))
			.then(entity =>  controllUtils.sendSuccess(res, entity))
			.catch(error =>  controllUtils.sendError(res, error)),
};