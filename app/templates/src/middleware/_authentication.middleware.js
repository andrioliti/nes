const { jwt: jwtUtils } = require('../security');
const sessionDao = require('../dao/session.dao');

const sendStatus = (res, status, obj) => res.status(status).json(obj);
const unauthorized = res => sendStatus(res, 401, { error: 'Unauthorized!' });
const error = res => sendStatus(res, 500, { error: 'Invalid token!' });

module.exports = async (req, res, next) => {
  try {
    if (!req.headers.authorization)
      return sendStatus(res, 400, { error: 'Bad request!'});

    const token = req.headers.authorization.split(' ')[1];
    
    jwtUtils.verify(token);

    const session = await sessionDao.findSessionWithToken(token);
    
    if (session && session.expiredAt >= new Date()) {
      next();
    } else {
      return unauthorized(res);
    }
  } catch (e) {
    return error(res);
  }
};