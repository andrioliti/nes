const crypto        = require('crypto');
const config        = require('../utils/config.utils.js');
const digestType    = 'base64';
const jwt           = require('jsonwebtoken');
const ALGORITHM_JWT = 'HS512'

const sha = phrase => {
  const hmac = crypto.createHmac(config.chipherMethod, config.secret);

  return hmac.update(phrase)
    .digest(digestType)
    .toString();
};

const generate = 
  object => 
    jwt.sign(object, config.secret, {algorithm: ALGORITHM_JWT});

const verify = 
  token => 
    jwt.verify(token, config.secret, {algorithm: ALGORITHM_JWT});

module.exports = {
  utils: {
    sha,
  }, 
  jwt: {
    generate,
    verify,
  }
};