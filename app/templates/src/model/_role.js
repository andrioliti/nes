const Sequelize = require('sequelize');

module.exports = sequelize => {
  const Role = sequelize.define('role', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notNull: true,
      },
    }, 
    value: {
      type: Sequelize.INTEGER,
      allowNull: false,
      validate: {
        notNull: true,
      },
      unique: true,
    },
    desc: Sequelize.STRING,
  }, {
    timestamps: true,
    paranoid: true,
    underscored: true,
  });

  Role.associate = models => {
    Role.hasMany(models.user, {
      as: 'users'
    });
  };

  return Role;
};