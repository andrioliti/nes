const Sequelize = require('sequelize');
const { utils: securityUtils } = require('../security/index');

const hashPassword =  entity => {
  if (entity.changed('password')) {
    entity.password = securityUtils.sha(entity.password);
  }
};

module.exports = sequelize => {
  const User = sequelize.define('user', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    fullName: {
      type: Sequelize.STRING,
      field: 'full_name',
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        isEmail: true,
      },
    },
    login: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notNull: true,
      },
      unique: true,
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notNull: true,
      },
    },
  }, {
    timestamps: true,
    paranoid: true,
    underscored: true,
  });

  User.beforeCreate(hashPassword);
  User.beforeUpdate(hashPassword);

  User.associate = models => {
    User.belongsTo(models.role, {
      as: 'role',
      constraints: true,
    });

    User.hasMany(models.session, {
      as: 'sessions',
      allowNull: false,
    });
  };
 
  User.prototype.validatePassword = (validatePass, entityPass) => {
    return securityUtils.sha(validatePass) == entityPass;
  };

  return User;
};