const Sequelize = require('sequelize');

module.exports = sequelize => {
  const Session = sequelize.define('session', {
    token: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notNull: true,
      },
    },
    expiredAt: {
      type: Sequelize.DATE,
      field: 'expired_at',
      allowNull: false,
      validate: {
        notNull: true,
      },
    },
  }, {
    timestamps: true,
    paranoid: true,
    underscored: true,
  });

  Session.associate = models => {
    Session.belongsTo(models.user, {
      as: 'user',
      constraints: true,
    });
  };

  return Session;
};