const express = require('express');
const router = express.Router();

const authController = require('../controller/auth.controller');

router.post('/login', authController.login);
router.post('/refresh', authController.refresh);
router.post('/validate', authController.validate);
router.post('/logout', authController.logout);

module.exports = router;
