const express = require('express');
const router = express.Router();

const sessionController = require('../controller/session.controller');

router.get('/', sessionController.findAll);
router.post('/', sessionController.create);
router.put('/', sessionController.update);
router.delete('/:id', sessionController.delete);

module.exports = { 
  router: router,
  path: '/session' 
};
