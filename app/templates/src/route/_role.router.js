const express = require('express');
const router = express.Router();

const roleController = require('../controller/role.controller');

router.get('/', roleController.findAll);
router.post('/', roleController.create);
router.put('/', roleController.update);
router.delete('/:id', roleController.delete);

module.exports = { 
  router: router,
  path: '/role' 
};
