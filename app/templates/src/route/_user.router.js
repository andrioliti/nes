const express = require('express');
const router = express.Router();

const userController = require('../controller/user.controller');

router.get('/', userController.findAll);
router.post('/', userController.create);
router.put('/', userController.update);
router.delete('/:id', userController.delete);
router.get('/:id', userController.getAllSession);

module.exports = { 
  router: router,
  path: '/user' 
};
