const fs = require('fs');
const path = require('path');
const express = require('express');

const router = express.Router();
const basename = path.basename(__filename);

fs
  .readdirSync(__dirname)
  .filter(file => 
    (file.indexOf('.') !== 0) && 
    (file !== basename) && 
    (file.slice(-3) === '.js') &&
    (file.indexOf('auth') === -1))
  .forEach(file => {
    let rt = require(path.join(__dirname, file));
    router.use(rt.path, rt.router);  
  });

module.exports = router;
