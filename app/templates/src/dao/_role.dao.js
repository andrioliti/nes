const abstractDao = require('./abstract.dao')('role');
const entityModel = abstractDao.getEntityModel();

module.exports = {
  ...abstractDao,
}