const abstractDao = require('./abstract.dao')('session');
const sessionDao = abstractDao.getEntityModel();

const findSessionWithToken = token => sessionDao.findOne({ where: { token } });

const deleteByUserId = userId => sessionDao.destroy({ where: { userId }});

module.exports = {
  ...abstractDao,

  findSessionWithToken,
  deleteByUserId,

  findAll: (query) => sessionDao.findAll({ where: query, include: ['user'] }),
};