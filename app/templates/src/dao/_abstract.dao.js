module.exports = entityName => {
  const entityModel = require('../model/index')[entityName];

  return {
    findAll: query => entityModel.findAll({ where: query }),

    update: entity => entityModel.findOne({ where: { id: entity.id } })
      .then(ret => ret.update(entity)),

    create: entity => entityModel.create(entity),

    delete: id => entityModel.findOne({ where: { id } })
      .then(entity => entity.destroy()),

    findById: (id, include) => entityModel.findOne({ where: { id }, include }),

    getEntityModel: () => entityModel,
  }
};