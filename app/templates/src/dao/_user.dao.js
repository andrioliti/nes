const abstractDao = require('./abstract.dao')('user');
const model = abstractDao.getEntityModel();

module.exports = {
  ...abstractDao,

  getAllSession: (id) => model.findOne({ where: { id }})
      .then(entity => entity.getSessions()),
  
  findAll: (query) => model.findAll({ where: query, include: ['role'] }),

  findByLogin: (login) => model.findOne({ where: { login }, include: ['role'] }),

  validatePassword: (validatePass, entityPass) => model.validatePassword(validatePass, entityPass),
};