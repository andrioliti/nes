const Generator = require('yeoman-generator');
const yosay = require('yosay');
const _ = require('lodash');
const pluralize = require('pluralize');

const RELATION_FLOW = [{
    name: 'belogs to',
    value: 'BELONGS_TO',
}, {
    name: 'has many',
    value: 'HAS_MANY', 
}];

const DATE_TYPES = [{
        name: 'CHAR',
        value: 'CHAR',
    }, {
        name: 'STRING',
        value: 'STRING',
    }, {
        name: 'TEXT',
        value: 'TEXT',
    }, {
        name: 'INTEGER',
        value: 'INTEGER'
    },{
        name: 'BIGINT',
        value: 'BIGINT'
    },{
        name: 'FLOAT',
        value: 'FLOAT'
    },{
        name: 'REAL',
        value: 'REAL'
    },{
        name: 'DOUBLE',
        value: 'DOUBLE'
    },{
        name: 'TIME',
        value: 'TIME'
    },{
        name: 'DATETIME',
        value: 'DATETIME'
    },{
        name: 'DATEONLY',
        value: 'DATEONLY'
    },{
        name: 'SERIAL',
        value: 'SERIAL'
    }];

module.exports = class extends Generator {

    constructor(args, opts) {
        super(args, opts);

        this.argument("appname", { type: String, required: true });
        this.option("in", { type: String, description: 'Input file that define database model in json format.' });
        this.option("i", { description: 'Run npm install after code generator complete.' });
        this.option("a", { description: 'Add utilities for auth functions.' });

        this.tables = [];

        // this.tables = [{
        //     tableName: 'User',
        //     clomuns: [{
        //         columnName: 'name',
        //         columnType: 'STRING',
        //         required: true
        //     }]
        // }, {
        //     tableName: 'Message',
        //     clomuns: [{
        //         columnName: 'description',
        //         columnType: 'STRING'
        //     }]
        // }];
    }

    async prompting() {
        this.log(yosay('all table contain id, created and updated fields.'));
    
        this.tables = [];
        this.relations = [];

        const getTable = () => {
            return this.prompt({
                type: 'input',
                name: 'tableName',
                message: 'Whats your table name?'
            }).then(props => {
                return getColumn({ 'tableName': props.tableName, columns: [] });
            });
        }

        const addNextTable = () => {
            return this.prompt({
                    type: 'confirm',
                    name: 'addTable',
                    message: 'Do you want add new table?'
                }).then(props => props.addTable ? getTable() : this.tables.length >= 2 ? addNextRelation() : null);
        }

        const addNextColumn = table => {
            return this.prompt({
                type: 'confirm',
                name: 'addColumn',
                message: 'Do you want add new column?'
            }).then(props => {
                if (props.addColumn) {
                    return getColumn(table);
                } else {
                    this.tables.push(table);
                    return addNextTable();
                }
            });
        }

        const getColumn = table => {
            return this.prompt([{
                type: 'input',
                name: 'columnName',
                message: 'What is the name of this column?'
            }, {
                type: 'list',
                name: 'type',
                message: 'What kind of this column?',
                choices: DATE_TYPES
            }, {
                type: 'confirm',
                name: 'columnRequired',
                message: 'This column is required?'
            }]).then(props => {
                table.columns.push({
                    columnName: props.columnName,
                    columnType: props.type,
                    required: props.columnRequired
                });

                return addNextColumn(table);
            });
        }

        const addNextRelation = () => 
            this.prompt({
                type: 'confirm',
                name: 'addRelation',
                message: 'Do you want add new relation?'
            }).then(props => props.addRelation ? getRelation() : null); 

        const getRelation = () =>
            this.prompt([{
                type: 'list',
                name: 'rigthEntity',
                message: 'Select your rigth entity',
                choices: this.tables.map(t => t.tableName),
            }, {
                type: 'list',
                name: 'dataFlow',
                message: 'Select your data direction of the relationship',
                choices: RELATION_FLOW,
            }, {
                type: 'list',
                name: 'leftEntity',
                message: 'Select your belong entity',
                choices: this.tables.map(t => t.tableName),
            },]).then(props => {
                const entry = {
                    rigth: props.rigthEntity,
                    left: props.leftEntity,
                    flow: props.dataFlow,
                };
                this.relations.push(entry);

                return addNextRelation();
            });
        
        if (this.options['in']) {
            const inputFilePath = this.destinationPath(this.options['in']);
            if (this.fs.exists(inputFilePath)) {
                this.tables = this.fs.readJSON(inputFilePath);
                return;
            } else {
                this.log.error(`File ${inputFilePath} not found.`);
                process.exit(1);
            }
        } else {
            return getTable();
        }
    }

    paths() {
        this.destinationRoot(_.snakeCase(this.options.appname));
    }

    async writing() {
        //package.json
        this.fs.copyTpl(
            this.templatePath('_package.json'),
            this.destinationPath('package.json'),
            {
                appname: this.options.appname
            });

        //utis/http.message
        this.fs.copyTpl(
            this.templatePath('_http.message.js'),
            this.destinationPath('src/utils/http.message.js'), { });
        
        //www
        this.fs.copyTpl(
            this.templatePath('_www'),
            this.destinationPath('bin/www'), {});

        //config
        this.fs.copyTpl(
            this.templatePath('_config.ejs'),
            this.destinationPath('config/config.js'), {
                appname: _.snakeCase(this.options.appname),
                auth: this.options['a'],
            });

        //app
        this.fs.copyTpl(
            this.templatePath('_app.ejs'),
            this.destinationPath('app.js'), {
                auth: this.options['a']
            });

        //sequelizerc
        this.fs.copyTpl(
            this.templatePath('_sequelizerc'),
            this.destinationPath('.sequelizerc'), {});

        //router/index
        this.fs.copyTpl(
            this.templatePath('src/route/_index.js'),
            this.destinationPath('src/route/index.js'), {});

        //model/index
        this.fs.copyTpl(
            this.templatePath('src/model/_index.js'),
            this.destinationPath('src/model/index.js'), {});

        //dao/abstract.dao
        this.fs.copyTpl(
            this.templatePath('src/dao/_abstract.dao.js'),
            this.destinationPath('src/dao/abstract.dao.js'), {});

        //controller/abstract.controller
        this.fs.copyTpl(
            this.templatePath('src/controller/_abstract.controller.js'),
            this.destinationPath('src/controller/abstract.controller.js'), {});

        //controller/controller.utils
        this.fs.copyTpl(
            this.templatePath('src/controller/_controller.utils.js'),
            this.destinationPath('src/controller/controller.utils.js'), {});
        
        //docker-compose
        this.fs.copyTpl(
            this.templatePath('_docker-compose.ejs'),
            this.destinationPath('docker-compose.yml'), {
                appname: _.snakeCase(this.options.appname)
            });
        
        //envs/database.env
        this.fs.copyTpl(
            this.templatePath('envs/_database.env.ejs'),
            this.destinationPath('envs/database.env'), {
                appname: _.snakeCase(this.options.appname)
            });
            
        for (let index in this.tables) {
            let table = this.tables[index];
            let modelName = table.tableName.toLowerCase();
            let pModelName = pluralize(modelName);
            let cModelName = _.capitalize(modelName);

            //Model
            this.fs.copyTpl(
                this.templatePath('src/model/_model.ejs'),
                this.destinationPath(`src/model/${modelName}.model.js`), {
                    cModelName,
                    pModelName,
                    columns: table.columns.map(c => ({
                        columnName: _.camelCase(c.columnName),
                        columnType: c.columnType,
                    })),
                });
            
            //Dao
            this.fs.copyTpl(
                this.templatePath('src/dao/_dao.ejs'),
                this.destinationPath(`src/dao/${modelName}.dao.js`), {
                    pModelName,
                });
            
            //controller
            this.fs.copyTpl(
                this.templatePath('src/controller/_controller.ejs'),
                this.destinationPath(`src/controller/${modelName}.controller.js`), {
                    modelName,
                });

            //route
            this.fs.copyTpl(
                this.templatePath('src/route/_router.ejs'),
                this.destinationPath(`src/route/${modelName}.router.js`), {
                    modelName,
                    pModelName,
                });

            //migration create
            this.fs.copyTpl(
                this.templatePath('db/migrations/_migration.ejs'),
                this.destinationPath(`db/migrations/${_.now()}-create-${modelName}.js`), {
                    modelName,
                    pModelName,
                    columns: table.columns.map(c => ({
                        columnName: _.camelCase(c.columnName),
                        columnType: c.columnType,
                        field: _.snakeCase(c.columnName)
                    })),
                });
        }

        if (this.options['a']) {
            //models
            this.fs.copyTpl(
                this.templatePath('src/model/_role.js'),
                this.destinationPath(`src/model/role.js`), {});
            this.fs.copyTpl(
                this.templatePath('src/model/_session.js'),
                this.destinationPath(`src/model/session.js`), {});
            this.fs.copyTpl(
                this.templatePath('src/model/_user.js'),
                this.destinationPath(`src/model/user.js`), {});
            //dao
            this.fs.copyTpl(
                this.templatePath('src/dao/_role.dao.js'),
                this.destinationPath(`src/dao/role.dao.js`), {});
            this.fs.copyTpl(
                this.templatePath('src/dao/_session.dao.js'),
                this.destinationPath(`src/dao/session.dao.js`), {});
            this.fs.copyTpl(
                this.templatePath('src/dao/_user.dao.js'),
                this.destinationPath(`src/dao/user.dao.js`), {});
            //controller
            this.fs.copyTpl(
                this.templatePath('src/controller/_role.controller.js'),
                this.destinationPath(`src/controller/role.controller.js`), {});
            this.fs.copyTpl(
                this.templatePath('src/controller/_session.controller.js'),
                this.destinationPath(`src/controller/session.controller.js`), {});
            this.fs.copyTpl(
                this.templatePath('src/controller/_user.controller.js'),
                this.destinationPath(`src/controller/user.controller.js`), {});
            this.fs.copyTpl(
                this.templatePath('src/controller/_auth.controller.js'),
                this.destinationPath(`src/controller/auth.controller`), {});
            //middleware
            this.fs.copyTpl(
                this.templatePath('src/middleware/_authentication.middleware.js'),
                this.destinationPath(`src/middleware/authentication.middleware.js`), {});
            //route
            this.fs.copyTpl(
                this.templatePath('src/route/_role.router.js'),
                this.destinationPath(`src/route/role.router.js`), {});
            this.fs.copyTpl(
                this.templatePath('src/route/_session.router.js'),
                this.destinationPath(`src/route/session.router.js`), {});
            this.fs.copyTpl(
                this.templatePath('src/route/_user.router.js'),
                this.destinationPath(`src/route/user.router.js`), {});
            this.fs.copyTpl(
                this.templatePath('src/route/_auth.router.js'),
                this.destinationPath(`src/route/auth.router.js`), {});
            //migrations
            this.fs.copyTpl(
                this.templatePath('db/migrations/_00000000000001-create-user.js'),
                this.destinationPath(`db/migrations/00000000000001-create-user.js`), {});
            this.fs.copyTpl(
                this.templatePath('db/migrations/_00000000000002-create-role-entity.js'),
                this.destinationPath(`db/migrations/00000000000002-create-role-entity.js`), {});
            this.fs.copyTpl(
                this.templatePath('db/migrations/_00000000000003-create-session-entity.js'),
                this.destinationPath(`db/migrations/00000000000003-create-session-entity.js`), {});
            this.fs.copyTpl(
                this.templatePath('db/migrations/_00000000000004-user-to-role.js'),
                this.destinationPath(`db/migrations/00000000000004-user-to-role.js`), {});
            this.fs.copyTpl(
                this.templatePath('db/migrations/_00000000000005-user-to-session.js'),
                this.destinationPath(`db/migrations/00000000000005-user-to-session.js`), {});
            //sedders
            this.fs.copyTpl(
                this.templatePath('db/seeders/_00000000000001-role-admin.js'),
                this.destinationPath(`db/seeders/00000000000001-role-admin.js`), {});
            this.fs.copyTpl(
                this.templatePath('db/seeders/_00000000000002-user-admin.js'),
                this.destinationPath(`db/seeders/00000000000002-user-admin.js`), {});
            //utils
            this.fs.copyTpl(
                this.templatePath('src/security/_index.js'),
                this.destinationPath(`src/security/index.js`), {});
            this.fs.copyTpl(
                this.templatePath('src/utils/_config.utils.js'),
                this.destinationPath(`src/utils/config.utils.js`), {});

            if (this.relations) {
                //migration for relations
                this.relations.forEach(relation => {
                    let destinationPath = `db/migrations/${_.now()}-${standardizeTableNameForFile(relation.rigthEntity)}-to-${standardizeTableNameForFile(relation.leftEntity)}.js`;
                    let rigthEntityTableName = `${standardizeTableName(relation.rigthEntity)}`;
                    let leftEntityTableName = `${standardizeTableName(relation.leftEntity)}`;
                    let leftEntityId = `${_.snakeCase(relation.leftEntity)}_id`;

                    if (relation.flow === RELATION_FLOW[1].value) {
                        destinationPath = `db/migrations/${_.now()}-${standardizeTableNameForFile(relation.leftEntity)}-to-${standardizeTableNameForFile(relation.rigthEntity)}.js`;
                        rigthEntityTableName = `${standardizeTableName(relation.leftEntity)}`;
                        leftEntityTableName = `${standardizeTableName(relation.rigthEntity)}`;
                        leftEntityId = `${_.snakeCase(relation.leftEntity)}_id`;
                    } 

                    this.fs.copyTpl(
                        this.templatePath('db/_migrations/_relation_migration.ejs'),
                        this.destinationPath(destinationPath), {
                            rigthEntityTableName,
                            leftEntityTableName,
                            leftEntityId,        
                        });
                });
            } 
        }

        const standardizeTableName = (tableName) => pluralize(_.snakeCase(tableName));

        const standardizeTableNameForFile = (tableName) => pluralize(_.kebabCase(tableName));
    }

    install() {
        if (this.options.i) {
            this.npmInstall();
        }
    }
};